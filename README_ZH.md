# Linguee

简体中文 | [English](./README.md)

除 Kyori/AdventureAPI 之外的又一个不一样的文本组件方案 ([Text](./api/src/main/java/io/ib67/bukkit/chat/Text.java) ).

```java
TextThemes.DEFAULT = TextThemes.worldguard; // 设置主题配色
Text.of("**欢迎回来!** `{{papi:player_name}}`, 您有 *{{ mailcount }}* 封未读邮件, ") // 支持使用 PAPI 和自定义占位符
     .concat( // 与其他文本拼接
             Text.of("[点击此处](/kick {{papi:player_name}}) 来查看邮箱.") // 支持使用类 Markdown 的语法描述文本, 像是 [外观](URL/指令) 和 *斜体*
                .withClickAction(sender -> { // 如果玩家点击到 `点击此处` 之外的地方
                    Text.of("&c请点击下划线的地方, *别点我!*").send(sender);
                    return true; // 返回 true 后,这段 handler 将不会再被调用. (除非是另一个 `send` 出来的信息)
                })
        ).send(player,Papi.IT.and(this::processMailCount).and(...)); // 发送给玩家,同时使用 PAPI 渲染文本
```
![Image of the code](./dist/2022-07-09_19-53.png)

# 开始使用
```groovy
repositories {
    mavenCentral()
}
def lingueeVersion = "0.2.0"
dependencies {
    implementation "io.ib67.linguee:api:$lingueeVersion"
    implementation "io.ib67.linguee:integration-papi:$lingueeVersion" // 如果需要使用 PAPI 集成
}
shadowJar {
    relocate 'io.ib67.bukkit.chat', 'your-package-name.linguee' // 不要忘记 relocate
}
```

把这段代码加入到 `onEnable` 中:
```java
Linguee.getInstance().init(this); // 初始化 Linguee
```
# Features

Linguee 可以优雅的处理文本.

## 多彩文本

Linguee 支持经典的颜色文本写法

`&a Green &b Aqua &c Red &r Reset ....`

同时也支持一些类似 `gradient` 的操作符.

`&<gr:aqua-0xFFFF>Texts which are colored from aqua to red` (渐变颜色暂未支持... 但是支持使用 RGB 颜色: `&<1f1e33>`)

## 文本内 DSL

类似 Markdown.

`你可以点击[此处](/help)得到帮助.` -> `你可以点击&n此处&r得到帮助.` (`此处`可点击, 也可以使用网址.)

``Use a `Sticky Wand` to attack them!`` -> `Use a &oSticky Wand&r to attack them!`

`*Sorry!* But you cannot build in *others' residences.` -> `&lSorry! &rBut you cannot build in &bothers' rediences.`

## 文本主题化

使用主题化的文本, 你不再需要考虑配色问题, 以此把更多的时间花在如何精确的传达语意上.

``Use a `Sticky Wand` to attack them!`` -> `Use a &oSticky Wand&r to attack them!`
-> `&bUse a &e&oSticky Wand&r&b to attack them!`

## 模板

你可以在文本内使用模板语法, 并且 Linguee 会保证模板内的文本不会污染到外部. (比如颜色)

`I think {{name}} is abusing his sword.` -> (With `{name = "&bnullcat_"}`) `I think &bnullcat_&r is abusing his sword.`

### 其他插件集成

Linguee 同时提供了 Placeholder API 的支持, 只需作为 Post Processor 传入 `send` 即可.

`Welcome back! {{papi:player_name}}` -> (With `{name = "nullcat_"}`) `Welcome back! nullcat_`

注意, 此功能需要添加 `io.ib67.linguee:integration-papi:$VERSION` 依赖.
